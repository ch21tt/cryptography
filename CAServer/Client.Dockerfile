FROM openjdk:18-alpine3.13

COPY . .
RUN javac Client.java

ENV SERVER_URL=localhost:5000
CMD ["java", "Client"]

